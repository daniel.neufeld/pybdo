# PyBDO

PyBDO is a class that implements the Possibilistic Design Optimization technique for handling optimization problems with uncertainties that can be characterized as fuzzy numbers.

## Dependencies
* python 3.6 +
* numpy
* scipy

## Optional Dependencies for Running the Example Problems
* matplotlib

## Optional Dependencies for Building the Docs
* sphinx
* sphinx_rtd_theme

See the [install.md](install.md) file for instructions on how to make sure the sphinx tutorial package is registered inside your python environment.

## Project Structure

- **docs**: Sphinx documentation files
- **PyBDO**: Main folder
  - **PyBDO.py**: Contains the PyBDO class
  - **PyBDO_example.py**: Script that contains two simple numerical example problems


## References

* TDOD
