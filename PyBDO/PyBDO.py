# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 13:35:30 2013

@author: daniel
"""
import numpy as np
from scipy.optimize import fsolve
from scipy.optimize import minimize, Bounds


class PBDO:
    def __init__(self, obj, ineqcons, params, args=(), alpha=0.0, maxIter=100,
                 tol=10**-5, iprint=1, optimizer='SLSQP', optimizerOpts=None,
                 PMAopts=None, callback=None, updateStart=True):
        self.optimizer = optimizer
        """
        PBDO Optimization class

        Parameters
        ----------

        obj : function
            objective function
        ineqcons : list of functions
            inequality constraints list
        params : parameters
            parameters class
        args : list
            additional arguments to be passed to objective function
        alpha : float
            alpha value
        dx : float
            dx value for gradient determination
        nCpu : integer
            number of CPU for parallel computation
        maxIter : integer
            maximum number of iterations
        tol : float
            tolerance
        iprint : integer
            display the result

        """
        self.tol = tol
        self.obj = obj
        self.args = args
        self.cons = ineqcons
        self.opts = optimizerOpts
        self.PMAopts = PMAopts

        self.alpha = alpha
        self.params = params
        self.maxIter = maxIter

        self.d0 = self.params.deterministicVars.d0
        self.dl = self.params.deterministicVars.lb
        self.du = self.params.deterministicVars.ub

        self.x0 = self.params.fuzzyVars.x0
        self.xl = self.params.fuzzyVars.lb
        self.xu = self.params.fuzzyVars.ub

        self.P0 = self.params.fuzzyParams.x0
        self.S = list()
        self.Pmpp = list()
        self.xMpp = list()
        self.xHistory = list()
        self.fHistory = list()
        self.dHistory = list()
        self.dMin = np.zeros(len(self.d0))
        self.xMin = np.zeros(len(self.x0))
        self.fMin = 0.0
        self.xHistory_full = list()
        self.fHistory_full = np.array([])
        self.callback = callback
        self.updateStart = updateStart

        for i in range(len(self.cons)):
            self.S.append(np.zeros([len(self.x0)]))
            self.Pmpp.append(self.P0)
            self.xMpp.append(self.x0)

    def solve(self):
        err = np.inf
        iMax = self.maxIter
        i = 1
        while err > self.tol and i <= iMax:
            print('ITERATION '+str(i))
            f1 = self.obj(self.d0, self.x0, self.P0, *self.args)
            self.deterministicPhase()
            self.possibilityAssessmentPhase()
            f2 = self.obj(self.dMin, self.xMin, self.P0, *self.args)
            err = np.abs(f2-f1)
            i += 1
            self.xHistory.append(self.x0)
            self.fHistory.append(f2)
            self.dHistory.append(self.d0)

    def set_d0(self, d0):
        assert len(d0) == len(self.params.deterministicVars.d0)
        assert len(d0) == len(self.params.deterministicVars.lb)
        assert len(d0) == len(self.params.deterministicVars.ub)
        self.params.deterministicVars.d0 = d0

    def set_x0(self, x0):
        assert len(self.params.fuzzyVars) == len(x0)
        for i in range(len(x0)):
            self.params.fuzzyVars[i].set_x0(x0)

    def set_alpha(self, alpha):
        self.alpha = alpha

    def deterministicPhase(self):
        """
        Runs a deterministic optimizaiton using shifted constraint and
        parameter values based on the possibility analysis of each constraint
        """
        nd = len(self.d0)  # Number of deterministic design variables
        nx = len(self.x0)  # Number of fuzzy design variables

        # joining fuzzy and deterministic
        dx0 = np.concatenate((self.d0, self.x0))
        dxLB = np.concatenate((self.dl, self.xl))  # variables and boundaries
        dxUB = np.concatenate((self.du, self.xu))

        mf = self.modifiedFunction(self.obj, nd, nx, np.zeros(
            len(self.x0)), self.P0, dxLB, dxUB)
        # Setting the modified objective funciton and constraint functions so
        # they are compatable with scipy optimizers
        obj = mf.modFcn
        cons = list()

        for i, con in enumerate(self.cons):
            mf = self.modifiedFunction(
                con, nd, nx, self.S[i], self.Pmpp[i], dxLB, dxUB)
            conFcn = mf.modFcn
            cons.append({'type': 'ineq', 'fun': conFcn, 'args': self.args})

        if self.callback != None:
            cb = self.modCallback(self.callback, dxLB, dxUB)
            callback = cb.modCall
        else:
            callback = None

        dxN = normalize(dx0, dxLB, dxUB)
        bounds = Bounds(np.zeros(len(dx0)), np.ones(len(dx0)))

        fmin = minimize(obj, dxN, bounds=bounds, constraints=cons, args=self.args,
                        method=self.optimizer, options=self.opts, callback=callback)
        dxMin = denormalize(fmin.x, dxLB, dxUB)
        d = dxMin[0:nd]
        x = dxMin[nd:]
        if self.updateStart:
            self.d0 = d
            self.x0 = x
        self.xMin = x
        self.dMin = d
        self.fMin = fmin.fun

    class modCallback:
        def __init__(self, callback, lb, ub):
            self.callback = callback
            self.lb = lb
            self.ub = ub

        def modCall(self, normVars):
            Vars = denormalize(normVars, self.lb, self.ub)
            self.callback(Vars)

    class modifiedFunction:
        def __init__(self, fcn, nd, nx, S, P, lb, ub):
            self.fcn = fcn
            self.nd = nd
            self.nx = nx
            self.P = P
            self.S = S
            self.lb = lb
            self.ub = ub

        def modFcn(self, dxN, *args):
            dx = denormalize(dxN, self.lb, self.ub)
            d = dx[0:self.nd]
            x = dx[self.nd:]
            return self.fcn(d, x+self.S, self.P, *args)

    def possibilityAssessmentPhase(self):
        nx = len(self.x0)
        nP = len(self.P0)
        xlb, x0, xub = self.params.fuzzyVars.getVectors(self.alpha, self.x0)
        Plb, P0, Pub = self.params.fuzzyParams.getVectors(self.alpha)

        xP0 = np.concatenate((x0, P0))
        xPLB = np.concatenate((xlb, Plb))
        xPUB = np.concatenate((xub, Pub))
        bounds = Bounds(np.zeros(len(xPLB)), np.ones(len(xPUB)))
        xP0_N = normalize(xP0, xPLB, xPUB)

        if self.callback != None:
            cb = self.modCallback(self.callback, xPLB, xPUB)
            callback = cb.modCall
        else:
            callback = None

        for i, con in enumerate(self.cons):
            print('POSSIBILITY ASSESSMENT: CONSTRAINT '+str(i))
            PMAfcn = self.PMA_modifiedFunctions(
                con, self.d0, nx, nP, xPLB, xPUB)
            PMAobj = PMAfcn.PMAobj
            fmin = minimize(PMAobj, xP0_N, bounds=bounds, args=self.args,
                            method=self.optimizer, options=self.PMAopts, callback=callback)
            xPmin = denormalize(fmin.x, xPLB, xPUB)
            xMPP = xPmin[0:nx]
            Pmpp = xPmin[nx:]
            self.Pmpp[i] = Pmpp
            self.xMpp[i] = xMPP
            self.S[i] = xMPP-self.x0

    class PMA_modifiedFunctions:
        def __init__(self, con, d, nx, nP, lb, ub):
            self.con = con
            self.nx = nx
            self.nP = nP
            self.d = d
            self.lb = lb
            self.ub = ub

        def PMAobj(self, xPN, *args):
            xP = denormalize(xPN, self.lb, self.ub)
            x = xP[0:self.nx]
            P = xP[0:self.nP]
            f = self.con(self.d, x, P, *args)
            return f

    def display(self):
        for i, f in enumerate(self.fHistory):
            print('{:<4d}f = {:<10.5f}  '.format(i, f), end="")
            print('x=[', end="")
            for j, x in enumerate(self.xHistory[i]):
                print('{:<6.4f}'.format(x), end=" ")
            print('] d=[', end="")
            for k, d in enumerate(self.dHistory[i]):
                print('{:<6.4f}'.format(d), end=" ")
            print(']')


class parameters:
    def __init__(self):
        self.fuzzyParams = fuzzyVector()
        self.fuzzyVars = fuzzyVector()
        self.deterministicVars = deterministicVars()

    def addDeterminisitcVar(self, d, lb, ub):
        self.deterministicVars.addVariable(d, lb, ub)

    def addFuzzyVar(self, fuzzyVar, lb, ub):
        self.fuzzyVars.add(fuzzyVar, lb, ub)

    def addFuzzyParam(self, fuzzyParam):
        self.fuzzyParams.add(fuzzyParam)


class deterministicVars:
    def __init__(self):
        self.d0 = np.array([])
        self.lb = np.array([])
        self.ub = np.array([])

    def addVariable(self, d, lb, ub):
        assert len(d) == len(lb)
        assert len(lb) == len(ub)
        if type(d) == type(float()):
            d = np.array([d])
        if type(lb) == type(float()):
            lb = np.array([lb])
        if type(ub) == type(float()):
            ub = np.array([ub])
        self.d0 = np.concatenate((self.d0, d))
        self.lb = np.concatenate((self.lb, lb))
        self.ub = np.concatenate((self.ub, ub))

    def __getitem__(self, k):
        return (self.d0[k], self.lb[k], self.ub[k])


class fuzzyVector:
    def __init__(self):
        self._list = list()
        self.lb = np.array([])
        self.ub = np.array([])
        self.x0 = np.array([])

    def add(self, fuzzyNumber, lb=None, ub=None):
        self._list.append(fuzzyNumber)
        self.lb = np.concatenate((self.lb, np.array([lb])))
        self.ub = np.concatenate((self.ub, np.array([ub])))
        self.x0 = np.concatenate((self.x0, np.array([fuzzyNumber.x0])))

    def getVectors(self, alpha, x=None):
        lb = np.zeros(len(self._list))
        ub = np.zeros(len(self._list))
        X = np.zeros(len(self._list))
        for i, fn in enumerate(self._list):
            if type(x) == type(None):
                I = fn.getInterval(alpha)
            else:
                I = fn.getInterval(alpha, x[i])
            lb[i] = I[0]
            X[i] = I[1]
            ub[i] = I[2]
        return lb, X, ub

    def display(self):
        lb, x, ub = self.getVectors(0.0)
        print(lb)
        print(x)
        print(ub)

    def __getitem__(self, k):
        return self._list[k]


class fuzzyNumber:
    """
    Class containing functions for defining and solving fuzzy numbers
    The input function must comply with the follwing definitions:
    1. Must have a maximal value of 1 at some x, called x0
    2. Must have an x-intercept at x<x0 called xMin
    3. Must have an x-intercept at x>x0 called xMax
    4. Must be continuous between xMin and xMax
    """

    def __init__(self, fcn):
        self.function = fcn
        self._delta = 0.0
        self._dx = 0.001
        self._x0 = 0.0
        self.xMin = 0.0
        self.xMax = 0.0
        self._delta = 0.0
        self._find_x0()
        self._find_xMin()
        self._find_xMax()
        self.x0 = self._x0

    def set_x0(self, x0):
        self.x0 = x0
        self._delta = x0-self._x0

    def _find_x0(self):
        self._x0 = fsolve(self._x0Eqn, 0.0)[0]

    def _find_xMin(self):
        self.xMin = fsolve(self.function, self._x0-self._dx)[0]

    def _find_xMax(self):
        self.xMax = fsolve(self.function, self._x0+self._dx)[0]

    def _x0Eqn(self, x):
        return self.function(x)-1.0

    def getInterval(self, alpha, x0=None):
        if x0 == None:
            x0 = self.x0
        else:
            self.set_x0(x0)
        dxR = (self.xMax+self._delta-self.x0)/2.
        dxL = (self.x0-self.xMin-self._delta)/2.
        if alpha == 0:
            interval = np.array(
                [self.xMin+self._delta, x0, self.xMax+self._delta])
        elif alpha < 1.0 and alpha > 0.0:
            xMin0 = fsolve(self._xEqn, self.xMin-dxL, args=alpha)[0]
            xMax0 = fsolve(self._xEqn, self.xMax+dxR, args=alpha)[0]
            interval = np.array([xMin0+self._delta, x0, xMax0+self._delta])
        elif alpha == 1.0:
            interval = np.array([x0, x0, x0])
        else:
            interval = None
            print('Error, 0<=alpha<=1')
        return interval

    def _xEqn(self, x, alpha):
        return self.function(x)-alpha

    def getV(self, x):
        I = self.getInterval(0.0)
        V = (x-I[0])/(I[1]-I[0])*2.0-1.0
        return V

    def getAlpha(self, x):
        x0 = x-self._delta
        alpha = self.function(x0)
        return alpha


class triangle:
    """
    Creates a triangular fuzzy function as follows
              _______________alpha=1
             /|\
            / | \
           /  |  \
          /   |   \
     lb__/    x    \__ub_ alhpa=0
    """

    def __init__(self):
        self.lb = 0.0
        self.ub = 0.0
        self.x = 0.0

    def getFuzzyNumber(self, lb, x0, ub):
        self.lb = lb
        self.ub = ub
        self.x = x0
        return fuzzyNumber(self.fcn)

    def fcn(self, x):
        mLHS = +1.0/(self.x-self.lb)
        bLHS = -self.lb*mLHS
        mRHS = -1.0/(self.ub-self.x)
        bRHS = -self.ub*mRHS
        if x < self.x:
            y = mLHS*x+bLHS
        if x > self.x:
            y = mRHS*x+bRHS
        if x == self.x:
            y = 1.0
        return y


def normalize(x, lb, ub):
    x, lb, ub = (np.array(x), np.array(lb), np.array(ub))
    return (x-lb)/(ub-lb)


def denormalize(x, lb, ub):
    x, lb, ub = (np.array(x), np.array(lb), np.array(ub))
    return x*(ub-lb)+lb
##################### EXAMPLE PROBLEM 1 #######################################


def test1_ob(d, x, P):
    f = x[0]+x[1]
    return f


def test1_C1(d, x, P):
    C1 = x[0]**2*x[1]/20.0-1.
    return C1


def test1_C2(d, x, P):
    C2 = (x[0]+x[1]-5.0)**2/30.+(x[0]-x[1]-12.0)**2/120.0-1.
    return C2


def test1_C3(d, x, P):
    C3 = 80.0/(x[0]**2+8.0*x[1]+5.0)-1
    return C3
##################### EXAMPLE PROBLEM 2 #######################################


def test2_ob(d, x, P):
    f = d[0]*d[1]
    return f


def test2_C1(d, x, P):
    C1 = P[2]-(600./d[0]/d[1]**2*P[0]+600./d[0]**2/d[1]*P[1])
    return C1


def test2_C2(d, x, P):
    D0 = 2.5
    L = 100.0
    C2 = D0-4.0*L**3/(P[3]*10.**7)/d[0]/d[1] * \
        ((P[0]/d[1]**2)**2+(P[1]/d[0]**2)**2)**.5
    return C2


def PBDOtest():
    from matplotlib import pyplot as plt
    from scipy.optimize import fsolve

################### SOLVING EXAMPLE PROBLEM 1 #################################
    def con1(x, y, conFcn):
        z = conFcn([], [x, y], [])
        return z

    def invcon1(y, x, conFcn):
        z = conFcn([], [x, y], [])
        return z

    plt.figure(1)
    tri = triangle()
    params = parameters()
    params.addFuzzyVar(tri.getFuzzyNumber(3.6, 5.0, 6.4), 0, 10)
    tri = triangle()
    params.addFuzzyVar(tri.getFuzzyNumber(4.3, 5.0, 5.7), 0, 10)
    opt = PBDO(test1_ob, (test1_C1, test1_C2, test1_C3), params, alpha=0.02)
    opt.solve()
    print('Example 1')
    opt.display()
    # Making the plots

    X1 = np.linspace(0.3, 10, 50)
    C1 = [fsolve(con1, 5, args=(x, test1_C1)) for x in X1]

    X2 = np.linspace(2.5, 9, 50)
    C2 = [fsolve(invcon1, 5, args=(x, test1_C2)) for x in X2]

    X3 = np.linspace(1, 9.3, 50)
    C3 = [fsolve(con1, 5, args=(x, test1_C3)) for x in X3]

    plt.plot(C1, X1)
    plt.plot(X2, C2)
    plt.plot(C3, X3)

    dMin = opt.xHistory[0]
    xMin = opt.xHistory[-1]
    M1, M2, M3 = opt.xMpp
    plt.plot(dMin[0], dMin[1], 'ro')
    plt.plot(xMin[0], xMin[1], 'bo')

    plt.plot(M1[0], M1[1], 'r*')
    plt.plot(M2[0], M2[1], 'r*')
    plt.plot(M3[0], M3[1], 'r*')

    x = [M1[0], M3[0], M3[0], M1[0], M1[0]]
    y = [M1[1], M1[1], M3[1], M3[1], M1[1]]
    plt.plot(x, y, '--k')
    plt.xlabel('x1')
    plt.ylabel('x2')
    plt.axis('equal')
    plt.legend(['constraint 1', 'constraint 2', 'constraint 3',
                'deterministic', 'reliable', 'MPP'])
    plt.title('Example 1 - Uncertain Design Variables')
#
#################### SOLVING EXAMPLE PROBLEM 2 ################################

    def con2(d1, d2, conFcn, P):
        z = conFcn([d1, d2], [], P)
        return z

    def invcon2(d2, d1, conFcn, P):
        z = conFcn([d1, d2], [], P)
        return z

    params = parameters()
    tri = triangle()
    params.addFuzzyParam(tri.getFuzzyNumber(900., 1000., 1100.))
    tri = triangle()
    params.addFuzzyParam(tri.getFuzzyNumber(400., 500., 600.))
    tri = triangle()
    params.addFuzzyParam(tri.getFuzzyNumber(38000., 40000., 42000.))
    tri = triangle()
    params.addFuzzyParam(tri.getFuzzyNumber(2.755, 2.900, 3.045))
    tri = triangle()

    params.addDeterminisitcVar([1., 3.], [0.1, 0.1], [10., 10.])

    cons = list()
    cons.append(test2_C1)
    cons.append(test2_C2)

    opt = PBDO(test2_ob, cons, params, alpha=0.05)
    # opt.possibilityAssessmentPhase()
    opt.solve()
    print('Example 2')
    opt.display()
    plt.figure(2)

    D1 = np.linspace(2.6, 5, 50)
    C1 = [fsolve(con2, 1, args=(d, test2_C1, opt.P0)) for d in D1]

    D2 = np.linspace(2.6, 5, 50)
    C2 = [fsolve(con2, 1, args=(d, test2_C2, opt.P0)) for d in D2]

    plt.plot(C1, D1)
    plt.plot(C2, D2)

    D1R = np.linspace(2.5, 5, 50)
    C1R = [fsolve(con2, 1, args=(d, test2_C1, opt.Pmpp[0])) for d in D1R]

    D2R = np.linspace(2.5, 5, 50)
    C2R = [fsolve(con2, 1, args=(d, test2_C2, opt.Pmpp[1])) for d in D2R]

    plt.plot(C1R, D1R, ':')
    plt.plot(C2R, D2R, ':')

    dMin = opt.dHistory[0]
    xMin = opt.dHistory[-1]
    M1, M2 = opt.Pmpp

    plt.plot(dMin[0], dMin[1], 'ro')
    plt.plot(xMin[0], xMin[1], 'bo')

    M1 = test2_C1(dMin, [], opt.Pmpp[0])
    M2 = test2_C2(dMin, [], opt.Pmpp[1])

    plt.xlabel('x1')
    plt.ylabel('x2')
    # plt.axis('equal')
    plt.legend(['constraint 1', 'constraint 2', 'moved constraint 1',
                'moved constraint 2', 'deterministic', 'reliable'])
    plt.title('Example 2 - Uncertain Parameters')
    plt.show()


if __name__ == '__main__':
    PBDOtest()
